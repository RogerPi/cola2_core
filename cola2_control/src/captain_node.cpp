/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <cola2_control/controllers/types.h>
#include <cola2_control/mission_utils/mission.h>
#include <cola2_lib/utils/filesystem.h>
#include <cola2_lib/utils/ned.h>
#include <cola2_lib_ros/diagnostic_helper.h>
#include <cola2_lib_ros/param_loader.h>
#include <cola2_lib_ros/serviceclient_helper.h>
#include <cola2_lib_ros/this_node.h>
#include <cola2_msgs/Action.h>
#include <cola2_msgs/CaptainStateFeedback.h>
#include <cola2_msgs/CaptainStatus.h>
#include <cola2_msgs/GoalDescriptor.h>
#include <cola2_msgs/Goto.h>
#include <cola2_msgs/KeyValue.h>
#include <cola2_msgs/Mission.h>
#include <cola2_msgs/MissionState.h>
#include <cola2_msgs/NavSts.h>
#include <cola2_msgs/Section.h>
#include <cola2_msgs/WorldSectionAction.h>
#include <cola2_msgs/WorldWaypointAction.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <ros/console.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <algorithm>
#include <cstdint>
#include <map>
#include <memory>
#include <numeric>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

// Helper functions to modify the key-value list
void setKeyValue(cola2_msgs::CaptainStateFeedback& fdbk, const std::string& key, const std::string& value)
{
  for (std::size_t i = 0; i < fdbk.keyvalues.size(); ++i)
  {
    if (fdbk.keyvalues[i].key == key)
    {
      fdbk.keyvalues[i].value = value;
      return;
    }
  }
  cola2_msgs::KeyValue keyvalue;
  keyvalue.key = key;
  keyvalue.value = value;
  fdbk.keyvalues.push_back(keyvalue);
}

/**
 * \brief Captain class. It can execute maneuvers like goto and keep position as well as missions
 */
class Captain
{
private:
  // ROS
  ros::NodeHandle nh_;
  ros::Subscriber sub_nav_;
  ros::Publisher pub_path_;
  ros::Publisher pub_captain_status_;
  ros::Publisher pub_captain_state_feedback_;
  ros::ServiceServer enable_goto_srv_;
  ros::ServiceServer disable_goto_srv_;
  ros::ServiceServer enable_section_srv_;
  ros::ServiceServer disable_section_srv_;
  ros::ServiceServer enable_mission_srv_;
  ros::ServiceServer pause_mission_srv_;
  ros::ServiceServer resume_mission_srv_;
  ros::ServiceServer disable_mission_srv_;
  ros::ServiceServer enable_keep_position_holonomic_srv_;
  ros::ServiceServer enable_keep_position_non_holonomic_srv_;
  ros::ServiceServer disable_keep_position_srv_;
  ros::ServiceServer enable_safety_keep_position_srv_;
  ros::ServiceServer disable_safety_keep_position_srv_;
  ros::ServiceServer disable_all_keep_positions_srv_;
  ros::ServiceServer reset_keep_position_srv_;
  ros::ServiceServer enable_external_mission_srv_;
  ros::ServiceServer disable_external_mission_srv_;
  ros::ServiceServer disable_all_and_set_idle_;
  ros::Timer main_timer_;
  ros::Timer diagnostics_timer_;

  // Diagnostics
  cola2::ros::DiagnosticHelper diagnostic_;

  // Actionlib client
  actionlib::SimpleActionClient<cola2_msgs::WorldWaypointAction> waypoint_actionlib_;
  actionlib::SimpleActionClient<cola2_msgs::WorldSectionAction> section_actionlib_;
  bool is_waypoint_actionlib_running_, is_section_actionlib_running_;

  // Possible captain states
  enum class CaptainStates
  {
    Idle,
    Goto,
    Section,
    Mission,
    KeepPosition,
    SafetyKeepPosition,
    ExternalMission
  };
  CaptainStates state_;
  std::string last_running_mission_;
  std::string external_mission_caller_name_;

  // Some keep position parameters
  bool last_keep_position_holonomic_;
  double last_keep_position_time_;
  double last_keep_position_duration_;

  // Feedback for the different modes
  cola2_msgs::CaptainStateFeedback goto_feedback_;
  cola2_msgs::CaptainStateFeedback section_feedback_;
  cola2_msgs::CaptainStateFeedback mission_feedback_;
  cola2_msgs::CaptainStateFeedback keep_position_feedback_;
  cola2_msgs::CaptainStateFeedback safety_keep_position_feedback_;
  std::uint16_t external_mission_feedback_id_;

  // Navigation data
  control::Nav nav_;
  double last_nav_received_;
  double last_ned_lat_origin_;
  double last_ned_lon_origin_;

  // Goto request
  cola2_msgs::Goto::Request goto_req_;

  // Section request
  cola2_msgs::Section::Request section_req_;

  // Maneuver flags
  bool waypoint_enabled_;
  bool section_enabled_;
  bool approach_completed_;
  bool step_completed_;

  // Internal list of missions
  struct MissionWithState
  {
    Mission mission;
    std::size_t current_step;
    ros::Time last_active;
    std::vector<double> step_timeouts;
    MissionWithState() : current_step(0)
    {
    }
  };
  std::map<std::string, MissionWithState> loaded_missions_;

  /**
   * \brief Waits for the actionlib to become ready
   * \param[in] Actionlib client
   * \param[in] Actionlib name (only used to display rosout msgs)
   */
  template <typename T>
  void waitActionlib(actionlib::SimpleActionClient<T>&, const std::string&);

  /**
   * \brief This method cancels the waypoint actionlib
   */
  void cancelWaypointActionlib();

  /**
   * \brief This method cancels the section actionlib
   */
  void cancelSectionActionlib();

  /**
   * \brief Callback to 'namespace'/navigator/navigation topic
   * \param[in] Navigation message
   */
  void updateNav(const cola2_msgs::NavSts&);

  /**
   * \brief Diagnostics timer
   */
  void diagnosticsTimer(const ros::TimerEvent&);

  /**
   * \brief Main timer. It triggers the main iteration
   */
  void mainTimer(const ros::TimerEvent&);

  /**
   * \brief Main iteration. It checks the current state and performs the required actions
   */
  void mainIteration();

  /**
   * \brief Given a mission builds a nav_msgs::Path to represent it in RViz
   * \param[in] Mission
   * \return Path
   */
  nav_msgs::Path createPathFromMission(Mission);

  /**
   * \brief Calls a standard action or a trigger service with name const std::string action_id and parameters
   *        const std::vector<std::string> parameters
   * \param[in] Is the action empty?
   * \param[in] Action ID
   * \param[in] Action parameters
   */
  void callAction(bool, const std::string&, std::vector<std::string>);

  /**
   * \brief This method sets to false all maneuver flags
   */
  void resetManeuverFlags();

  /**
   * \brief Use this method to remove a mission from the loaded missions list
   * \param[in] Mission name
   * \return Returns true if the mission was found and thus deleted and false otherwise
   */
  bool deleteLoadedMission(const std::string&);

  /**
   * \brief This method returns the default mission name
   * \return It returns the default mission name if it is available, or an empty string otherwise
   */
  std::string getDefaultMissionName();

  /**
   * \brief Execute all pending actions
   * \param[in] Mission index
   */
  void executePendingActions(std::string);

  /**
   * \brief This method returns the timeout of a goto
   * \param[in] Goto request
   * \param[out] Timeout
   * \param[out] Distance
   */
  void computeGotoTimeoutAndDistance(cola2_msgs::Goto::Request&, double&, double&);

  /**
   * \brief This method checks and enables a goto request
   * \param[in] Request
   * \param[out] Response
   * \param[in] Timeout
   * \return Success
   */
  bool enableGoto(cola2_msgs::Goto::Request&, cola2_msgs::Goto::Response&);

  /**
   * \brief Main goto method that is called at each iteration from the main timer
   */
  void goTo();  // Should be goto, but it is a keyword

  /**
   * \brief This method checks the waypoint actionlib to see if it has finalized
   * \return It returns true if it has finalized and false otherwise
   */
  bool gotoHasFinished();

  /**
   * \brief This method returns the timeout of a section
   * \param[in] Section request
   * \param[out] Timeout
   * \param[out] Distance
   */
  void computeSectionTimeoutAndDistance(cola2_msgs::Section::Request&, double&, double&);

  /**
   * \brief This method checks and enables a section request
   * \param[in] Section
   * \param[in] Timeout
   * \return Success
   */
  bool enableSection(cola2_msgs::Section::Request&, cola2_msgs::Section::Response&);

  /**
   * \brief Main section method that is called at each iteration from the main timer
   */
  void section();

  /**
   * \brief This method checks the section actionlib to see if it has finalized
   * \return It returns true if it has finalized and false otherwise
   */
  bool sectionHasFinished();

  /**
   * \brief This method returns a list of the timeouts for every mission step
   * \param[in] Mission
   * \param[in] First step from which the timeouts must be computed
   * \return It returns the list of timeouts
   */
  std::vector<double> computeMissionTimeouts(Mission, std::size_t);

  /**
   * \brief This method checks and enables a mission request
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableMission(cola2_msgs::Mission::Request&, cola2_msgs::Mission::Response&);

  /**
   * \brief Main mission method that is called at each iteration from the main timer
   */
  void mission();

  /**
   * \brief This method checks the mission step count to see if it has finalized
   * \return It returns true if it has finalized and false otherwise
   */
  bool missionHasFinished();

  /**
   * \brief Main keep position method that is called at each iteration from the main timer
   */
  void keepPosition();

  /**
   * \brief Main safety keep position method that is called at each iteration from the main timer
   */
  void safetyKeepPosition();

  /**
   * \brief Enable goto maneuver
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableGotoSrv(cola2_msgs::Goto::Request&, cola2_msgs::Goto::Response&);

  /**
   * \brief Disable goto maneuver
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableGotoSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Enable section maneuver
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableSectionSrv(cola2_msgs::Section::Request&, cola2_msgs::Section::Response&);

  /**
   * \brief Disable section maneuver
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableSectionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Enable mission
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableMissionSrv(cola2_msgs::Mission::Request&, cola2_msgs::Mission::Response&);

  /**
   * \brief Disable mission
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableMissionSrv(cola2_msgs::Mission::Request&, cola2_msgs::Mission::Response&);

  /**
   * \brief This method disables an active mission
   */
  void disableMissionHelper();

  /**
   * \brief Resume mission
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool resumeMissionSrv(cola2_msgs::Mission::Request&, cola2_msgs::Mission::Response&);

  /**
   * \brief Pause mission
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool pauseMissionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Enable keep position for surge, sway, heave and yaw DoFs
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableKeepPositionHolonomicSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Enable keep position for surge, heave, and yaw DoFs
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableKeepPositionNonHolonomicSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Disable keep position
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableKeepPositionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Enable safety keep position for surge, heave, and yaw DoFs
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableSafetyKeepPositionSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>&);

  /**
   * \brief Disable safety keep position
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableSafetyKeepPositionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Disable all keep positions
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableAllKeepPositionsSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);

  /**
   * \brief Reset keep position
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool resetKeepPositionSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>&);

  /**
   * \brief Allows an external controller to 'fake' that a mission is under execution
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool enableExternalMissionSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>&);

  /**
   * \brief Finalizes the exeternal mission
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableExternalMissionSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>&);

  /**
   * \brief Disables everything so that the state becomes idle
   * \param[in] Request
   * \param[out] Response
   * \return Success
   */
  bool disableAllAndSetIdleSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>&);

public:
  /**
   * \brief Class constructor
   */
  Captain();

  /**
   * \brief Class destructor
   */
  ~Captain();
};

Captain::Captain()
  : nh_("~")
  , diagnostic_(nh_, cola2::ros::getUnresolvedNodeName(), "soft")
  , waypoint_actionlib_("pilot/world_waypoint_req", true)
  , section_actionlib_("pilot/world_section_req", true)
  , is_waypoint_actionlib_running_(false)
  , is_section_actionlib_running_(false)
  , state_(CaptainStates::Idle)
  , last_nav_received_(0.0)
  , waypoint_enabled_(false)
  , section_enabled_(false)
  , approach_completed_(false)
  , step_completed_(false)
{
  // Init state feedback
  goto_feedback_.name = "Goto";
  section_feedback_.name = "Section";
  mission_feedback_.name = "Mission";
  keep_position_feedback_.name = "Keep position";
  safety_keep_position_feedback_.name = "Safety keep position";
  goto_feedback_.id = 0;
  section_feedback_.id = 0;
  mission_feedback_.id = 0;
  keep_position_feedback_.id = 0;
  safety_keep_position_feedback_.id = 0;
  external_mission_feedback_id_ = 0;

  // Publishers
  pub_path_ = nh_.advertise<nav_msgs::Path>("trajectory_path", 1, true);
  pub_captain_status_ = nh_.advertise<cola2_msgs::CaptainStatus>("captain_status", 1, true);
  pub_captain_state_feedback_ = nh_.advertise<cola2_msgs::CaptainStateFeedback>("state_feedback", 1, true);

  // Subscribers
  sub_nav_ = nh_.subscribe(cola2::ros::getNamespace() + "/navigator/navigation", 1, &Captain::updateNav, this);

  // Services
  enable_goto_srv_ = nh_.advertiseService("enable_goto", &Captain::enableGotoSrv, this);
  disable_goto_srv_ = nh_.advertiseService("disable_goto", &Captain::disableGotoSrv, this);
  enable_section_srv_ = nh_.advertiseService("enable_section", &Captain::enableSectionSrv, this);
  disable_section_srv_ = nh_.advertiseService("disable_section", &Captain::disableSectionSrv, this);
  enable_mission_srv_ = nh_.advertiseService("enable_mission", &Captain::enableMissionSrv, this);
  disable_mission_srv_ = nh_.advertiseService("disable_mission", &Captain::disableMissionSrv, this);
  pause_mission_srv_ = nh_.advertiseService("pause_mission", &Captain::pauseMissionSrv, this);
  resume_mission_srv_ = nh_.advertiseService("resume_mission", &Captain::resumeMissionSrv, this);
  enable_keep_position_holonomic_srv_ =
      nh_.advertiseService("enable_keep_position_holonomic", &Captain::enableKeepPositionHolonomicSrv, this);
  enable_keep_position_non_holonomic_srv_ =
      nh_.advertiseService("enable_keep_position_non_holonomic", &Captain::enableKeepPositionNonHolonomicSrv, this);
  disable_keep_position_srv_ = nh_.advertiseService("disable_keep_position", &Captain::disableKeepPositionSrv, this);
  enable_safety_keep_position_srv_ =
      nh_.advertiseService("enable_safety_keep_position", &Captain::enableSafetyKeepPositionSrv, this);
  disable_safety_keep_position_srv_ =
      nh_.advertiseService("disable_safety_keep_position", &Captain::disableSafetyKeepPositionSrv, this);
  disable_all_keep_positions_srv_ =
      nh_.advertiseService("disable_all_keep_positions", &Captain::disableAllKeepPositionsSrv, this);
  reset_keep_position_srv_ = nh_.advertiseService("reset_keep_position", &Captain::resetKeepPositionSrv, this);
  enable_external_mission_srv_ =
      nh_.advertiseService("enable_external_mission", &Captain::enableExternalMissionSrv, this);
  disable_external_mission_srv_ =
      nh_.advertiseService("disable_external_mission", &Captain::disableExternalMissionSrv, this);
  disable_all_and_set_idle_ = nh_.advertiseService("disable_all_and_set_idle", &Captain::disableAllAndSetIdleSrv, this);

  // Diagnostics timer
  diagnostics_timer_ = nh_.createTimer(ros::Duration(0.5), &Captain::diagnosticsTimer, this);

  // Main timer
  main_timer_ = nh_.createTimer(ros::Duration(0.5), &Captain::mainTimer, this);

  // Wait actionlib clients
  waitActionlib(waypoint_actionlib_, "waypoint");
  waitActionlib(section_actionlib_, "section");

  ROS_INFO_STREAM("Initialized");
}

Captain::~Captain()
{
  cancelWaypointActionlib();
  cancelSectionActionlib();
}

template <typename T>
void Captain::waitActionlib(actionlib::SimpleActionClient<T>& actionlib, const std::string& name)
{
  for (;;)
  {
    try
    {
      if (actionlib.waitForServer(ros::Duration(5.0)))
      {
        break;
      }
    }
    catch (const std::exception& ex)
    {
      ROS_WARN_STREAM("Actionlib's waitForServer() has thrown an exception "
                      << " (try updating your system): " << ex.what());
      ros::Duration(0.5).sleep();
    }
    ROS_INFO_STREAM("Waiting " << name << " actionlib");
  }
}

void Captain::cancelWaypointActionlib()
{
  if (is_waypoint_actionlib_running_)
  {
    waypoint_actionlib_.cancelGoal();
    bool result(false);
    while ((!result) && (!ros::isShuttingDown()))
    {
      result = waypoint_actionlib_.waitForResult(ros::Duration(0.1));
    }
  }
  is_waypoint_actionlib_running_ = false;
}

void Captain::cancelSectionActionlib()
{
  if (is_section_actionlib_running_)
  {
    section_actionlib_.cancelGoal();
    bool result(false);
    while ((!result) && (!ros::isShuttingDown()))
    {
      result = section_actionlib_.waitForResult(ros::Duration(0.1));
    }
  }
  is_section_actionlib_running_ = false;
}

void Captain::updateNav(const cola2_msgs::NavSts& msg)
{
  // Keep navigation info
  nav_.x = msg.position.north;
  nav_.y = msg.position.east;
  nav_.z = msg.position.depth;
  nav_.yaw = msg.orientation.yaw;
  nav_.altitude = msg.altitude;
  last_ned_lat_origin_ = msg.origin.latitude;
  last_ned_lon_origin_ = msg.origin.longitude;
  last_nav_received_ = ros::Time::now().toSec();
}

void Captain::diagnosticsTimer(const ros::TimerEvent&)
{
  if (state_ == CaptainStates::KeepPosition)
  {
    diagnostic_.add("keep_position_enabled", "True");
  }
  else
  {
    diagnostic_.add("keep_position_enabled", "False");
  }

  if ((state_ == CaptainStates::Mission) || (state_ == CaptainStates::ExternalMission))
  {
    diagnostic_.add("trajectory_enabled", "True");
    diagnostic_.setLevel(diagnostic_msgs::DiagnosticStatus::OK);
  }
  else
  {
    diagnostic_.add("trajectory_enabled", "False");
    diagnostic_.setLevel(diagnostic_msgs::DiagnosticStatus::OK);
  }
}

void Captain::mainTimer(const ros::TimerEvent&)
{
  mainIteration();
}

void Captain::mainIteration()
{
  // Check navigation
  if (state_ != CaptainStates::Idle)
  {
    if (ros::Time::now().toSec() - last_nav_received_ > 5.0)
    {
      ROS_WARN_STREAM("Navigation too old or never received");
      // Disable everything?
    }
  }

  // Perform actions
  if (state_ == CaptainStates::Goto)
  {
    goTo();
    if (gotoHasFinished())
    {
      ROS_INFO_STREAM("Goto finalized");
      state_ = CaptainStates::Idle;

      // Feedback
      goto_feedback_.state = cola2_msgs::CaptainStateFeedback::SUCCESS;
      goto_feedback_.header.stamp = ros::Time::now();
      pub_captain_state_feedback_.publish(goto_feedback_);
      ++goto_feedback_.id;
    }
  }
  else if (state_ == CaptainStates::Section)
  {
    section();
    if (sectionHasFinished())
    {
      ROS_INFO_STREAM("Section finalized");
      state_ = CaptainStates::Idle;

      // Feedback
      section_feedback_.state = cola2_msgs::CaptainStateFeedback::SUCCESS;
      section_feedback_.header.stamp = ros::Time::now();
      pub_captain_state_feedback_.publish(section_feedback_);
      ++section_feedback_.id;
    }
  }
  else if (state_ == CaptainStates::Mission)
  {
    mission();
    if (missionHasFinished())
    {
      ROS_INFO_STREAM("Mission finalized");
      state_ = CaptainStates::Idle;
      deleteLoadedMission(last_running_mission_);

      // Feedback
      mission_feedback_.state = cola2_msgs::CaptainStateFeedback::SUCCESS;
      mission_feedback_.header.stamp = ros::Time::now();
      pub_captain_state_feedback_.publish(mission_feedback_);
      ++mission_feedback_.id;
    }
  }
  else if (state_ == CaptainStates::KeepPosition)
  {
    keepPosition();
    if (gotoHasFinished())
    {
      ROS_INFO_STREAM("KeepPosition finalized");
      state_ = CaptainStates::Idle;

      // Feedback
      keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::SUCCESS;
      keep_position_feedback_.header.stamp = ros::Time::now();
      pub_captain_state_feedback_.publish(keep_position_feedback_);
      ++keep_position_feedback_.id;
    }
  }
  else if (state_ == CaptainStates::SafetyKeepPosition)
  {
    safetyKeepPosition();
    if (gotoHasFinished())
    {
      ROS_ERROR_STREAM("SafetyKeepPosition finalized");  // This should not happen
      state_ = CaptainStates::Idle;

      // Feedback
      safety_keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::SUCCESS;
      safety_keep_position_feedback_.header.stamp = ros::Time::now();
      pub_captain_state_feedback_.publish(safety_keep_position_feedback_);
      ++safety_keep_position_feedback_.id;
    }
  }
  else if (state_ == CaptainStates::ExternalMission)
  {
    // Nothing
  }

  // Publish captain status
  cola2_msgs::CaptainStatus captain_status_msg;
  if (state_ == CaptainStates::Idle)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::IDLE;
  }
  else if (state_ == CaptainStates::Goto)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::GOTO;
  }
  else if (state_ == CaptainStates::Section)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::SECTION;
  }
  else if (state_ == CaptainStates::Mission)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::MISSION;
    captain_status_msg.message = "Mission name: " + last_running_mission_;
  }
  else if (state_ == CaptainStates::KeepPosition)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::KEEPPOSITION;
  }
  else if (state_ == CaptainStates::SafetyKeepPosition)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::SAFETYKEEPPOSITION;
  }
  else if (state_ == CaptainStates::ExternalMission)
  {
    captain_status_msg.state = cola2_msgs::CaptainStatus::EXTERNALMISSION;
    captain_status_msg.message = "External mission caller name: " + external_mission_caller_name_;
  }
  for (const auto& name_mission : loaded_missions_)
  {
    cola2_msgs::MissionState mission_state;
    mission_state.name = name_mission.first;
    mission_state.last_active = name_mission.second.last_active;
    mission_state.current_step = name_mission.second.current_step;
    captain_status_msg.loaded_missions.push_back(mission_state);
  }
  pub_captain_status_.publish(captain_status_msg);
}

void Captain::executePendingActions(std::string mission_name)
{
  Mission& mission = loaded_missions_[mission_name].mission;
  while (loaded_missions_[mission_name].current_step < mission.size())
  {
    auto step = mission.getStep(loaded_missions_[mission_name].current_step);
    std::vector<MissionAction> actions = step->getActions();
    for (const auto& action : actions)
    {
      if (ros::isShuttingDown())
      {
        return;
      }
      callAction(action.getIsEmpty(), action.getActionId(), action.getParameters());
      /*ros::Duration(2.0).sleep();*/
    }
    ++loaded_missions_[mission_name].current_step;
  }
}

std::string Captain::getDefaultMissionName()
{
  std::string output;
  std::string package;
  if (!cola2::ros::getParam("~vehicle_config_launch_mission_package", package))
  {
    ROS_ERROR_STREAM("Parameter vehicle_config_launch_mission_package not defined");
    return output;
  }
  std::string package_path = ros::package::getPath(package);
  if (package_path.empty())
  {
    ROS_ERROR_STREAM("Error defining mission package path");
    return output;
  }
  std::string mission_path = package_path + "/missions/last_mission.xml";
  if (!cola2::utils::isFileAccessible(mission_path))
  {
    ROS_ERROR_STREAM("Last mission path not accessible");
    return output;
  }
  if (cola2::utils::isSymlink(mission_path))
  {
    output = cola2::utils::getSymlinkTarget(mission_path);
  }
  else
  {
    output = "last_mission.xml";
  }
  return output;
}

nav_msgs::Path Captain::createPathFromMission(Mission mission)
{
  // Create path from mission using NED
  nav_msgs::Path path;
  path.header.stamp = ros::Time::now();
  path.header.frame_id = "world_ned";
  cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
  for (std::size_t i = 0; i < mission.size(); ++i)
  {
    geometry_msgs::PoseStamped pose;
    pose.header.frame_id = path.header.frame_id;
    double x, y, z;
    ned.geodetic2Ned(mission.getStep(i)->getManeuverPtr()->x(), mission.getStep(i)->getManeuverPtr()->y(), 0.0, x, y,
                     z);
    pose.pose.position.x = x;
    pose.pose.position.y = y;
    pose.pose.position.z = mission.getStep(i)->getManeuverPtr()->z();
    path.poses.push_back(pose);
  }
  return path;
}

void Captain::callAction(const bool is_trigger, const std::string& action_id, const std::vector<std::string> parameters)
{
  if (is_trigger)
  {
    ROS_INFO_STREAM("Calling trigger service with id " << action_id);
    ros::ServiceClient* action_client = new ros::ServiceClient(nh_.serviceClient<std_srvs::Trigger>(action_id));
    if (action_client->waitForExistence(ros::Duration(1.0)))
    {
      std_srvs::Trigger* params = new std_srvs::Trigger();
      bool* success = new bool();
      if (cola2::ros::callServiceWithTimeout(*action_client, params->request, params->response, *success, 5.0))
      {
        if (!success)
        {
          ROS_ERROR_STREAM("Trigger service call failed");
        }
        else
        {
          if (!params->response.success)
          {
            ROS_WARN_STREAM("Trigger service returned without success. Message: " << params->response.message);
          }
        }
        delete action_client;
        delete params;
        delete success;
      }
      else
      {
        ROS_ERROR_STREAM("Trigger service with id " << action_id << " did not return after a 5 sec. timeout. "
                                                    << "Detaching waiting thread and leaking memory");
      }
    }
    else
    {
      ROS_ERROR_STREAM("Trigger service with id " << action_id << " does not exist");
      delete action_client;
    }
  }
  else
  {
    ROS_INFO_STREAM("Calling action service with id " << action_id);
    ros::ServiceClient* action_client = new ros::ServiceClient(nh_.serviceClient<cola2_msgs::Action>(action_id));
    if (action_client->waitForExistence(ros::Duration(1.0)))
    {
      cola2_msgs::Action* params = new cola2_msgs::Action();
      bool* success = new bool();
      for (const auto& param : parameters)
      {
        params->request.param.push_back(param);
      }
      if (cola2::ros::callServiceWithTimeout(*action_client, params->request, params->response, *success, 5.0))
      {
        if (!success)
        {
          ROS_ERROR_STREAM("Action service call failed");
        }
        delete action_client;
        delete params;
        delete success;
      }
      else
      {
        ROS_ERROR_STREAM("Action service with id " << action_id << " did not return after a 5 sec. timeout. "
                                                   << "Detaching waiting thread and leaking memory");
      }
    }
    else
    {
      ROS_ERROR_STREAM("Action service with id " << action_id << " does not exist");
      delete action_client;
    }
  }
}

void Captain::computeGotoTimeoutAndDistance(cola2_msgs::Goto::Request& req, double& timeout, double& distance)
{
  timeout = 0.0;
  distance = 0.0;

  // Check req.reference to transform req.position to appropiate reference frame
  double final_x, final_y, final_z, min_x_vel, min_z_vel;
  if (req.reference == cola2_msgs::GotoRequest::REFERENCE_NED)
  {
    final_x = req.position.x;
    final_y = req.position.y;
  }
  else if (req.reference == cola2_msgs::GotoRequest::REFERENCE_GLOBAL)
  {
    // Convert waypoint data
    cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
    ned.geodetic2Ned(req.position.x, req.position.y, 0.0, final_x, final_y, final_z);
  }
  else
  {
    ROS_ERROR_STREAM("Invalid reference frame in computeGotoTimeoutAndDistance");
    return;
  }
  final_z = req.position.z;
  if (req.altitude_mode)
  {
    final_z = nav_.z + nav_.altitude - req.position.z;
  }

  // Compute min velocities
  double max_x_vel, max_z_vel;
  cola2::ros::getParam(cola2::ros::getNamespace() + "/pilot/goto/max_surge", max_x_vel, 0.1);
  cola2::ros::getParam(cola2::ros::getNamespace() + "/controller/max_velocity_z", max_z_vel, 0.1);
  min_x_vel = std::min(req.linear_velocity.x, max_x_vel);
  min_z_vel = std::min(req.linear_velocity.x, max_z_vel);

  // Compute distances
  double dist_xy = std::sqrt(std::pow(final_x - nav_.x, 2) + std::pow(final_y - nav_.y, 2));
  double dist_z = std::fabs(final_z - nav_.z);

  // Limit min velocities
  if (min_x_vel < 1e-3)
    min_x_vel = 1e-3;
  if (min_z_vel < 1e-3)
    min_z_vel = 1e-3;

  // Compute timeout
  double time_xy = 2.0 * dist_xy / min_x_vel;
  double time_z = 2.0 * dist_z / min_z_vel;
  timeout = std::max(time_xy, time_z) + 30.0;
  distance = std::sqrt(std::pow(dist_xy, 2) + std::pow(dist_z, 2));
}

bool Captain::enableGoto(cola2_msgs::Goto::Request& req, cola2_msgs::Goto::Response& res)
{
  // Create waypoint
  cola2_msgs::WorldWaypointGoal waypoint;
  waypoint.goal.priority = req.priority;
  waypoint.goal.requester = ros::this_node::getName();
  waypoint.altitude_mode = req.altitude_mode;
  waypoint.altitude = req.altitude;

  // Check req.reference to transform req.position to appropiate reference frame
  if (req.reference == cola2_msgs::GotoRequest::REFERENCE_NED)
  {
    waypoint.position.north = req.position.x;
    waypoint.position.east = req.position.y;
  }
  else if (req.reference == cola2_msgs::GotoRequest::REFERENCE_GLOBAL)
  {
    // Convert waypoint data
    cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
    double north, east, depth;
    ned.geodetic2Ned(req.position.x, req.position.y, 0.0, north, east, depth);
    waypoint.position.north = north;
    waypoint.position.east = east;
  }
  else
  {
    std::string msg("Invalid goto reference frame");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }

  // Check max distance to waypoint
  double distance_to_waypoint =
      std::sqrt(std::pow(waypoint.position.north - nav_.x, 2) + std::pow(waypoint.position.east - nav_.y, 2));

  // Load max distance to waypoint from param server and check request
  double max_distance_to_waypoint;
  cola2::ros::getParam("~max_distance_to_waypoint", max_distance_to_waypoint, 300.0);  // Default value
  if (!(req.disable_axis.x && req.disable_axis.y) && (distance_to_waypoint > max_distance_to_waypoint))
  {
    std::stringstream msg_ss;
    msg_ss << "Invalid request. Max distance to waypoint is " << max_distance_to_waypoint
           << " while requested waypoint distance is at " << distance_to_waypoint;
    ROS_ERROR_STREAM(msg_ss.str());
    res.message = msg_ss.str();
    res.success = false;
    return false;
  }

  // Copy data from request to our waypoint
  waypoint.position.depth = req.position.z;
  waypoint.orientation.yaw = req.yaw;
  waypoint.disable_axis.x = req.disable_axis.x;
  waypoint.disable_axis.y = req.disable_axis.y;
  waypoint.disable_axis.z = req.disable_axis.z;
  waypoint.disable_axis.roll = req.disable_axis.roll;
  waypoint.disable_axis.pitch = req.disable_axis.pitch;
  waypoint.disable_axis.yaw = req.disable_axis.yaw;
  waypoint.position_tolerance.x = req.position_tolerance.x;
  waypoint.position_tolerance.y = req.position_tolerance.y;
  waypoint.position_tolerance.z = req.position_tolerance.z;
  waypoint.orientation_tolerance.yaw = req.orientation_tolerance.yaw;
  waypoint.linear_velocity.x = req.linear_velocity.x;

  // Display warning if sway, heave or yaw velocities are given
  if (req.linear_velocity.y != 0.0 || req.linear_velocity.z != 0.0 || req.angular_velocity.yaw != 0.0)
  {
    ROS_WARN_STREAM("GOTO velocity can only be defined in surge. Heave, sway and yaw depend on pose controller");
  }

  // Choose WorldWaypointReq mode taking into account the disable_axis and tolerance
  if (req.keep_position && !req.disable_axis.x && req.disable_axis.y && !req.disable_axis.yaw)
  {
    // Non holonomic keep position. Anchor mode
    waypoint.controller_type = cola2_msgs::WorldWaypointGoal::ANCHOR;
  }
  else if (!req.disable_axis.x && req.disable_axis.y && !req.disable_axis.yaw)
  {
    // Goto using x and yaw, with or without z
    waypoint.controller_type = cola2_msgs::WorldWaypointGoal::GOTO;
  }
  else if (!req.disable_axis.x && !req.disable_axis.y)
  {
    // Holonomic goto with x and y, with or without z and yaw
    waypoint.controller_type = cola2_msgs::WorldWaypointGoal::HOLONOMIC_GOTO;
  }
  else if (req.disable_axis.x && req.disable_axis.y && !req.disable_axis.z)
  {
    // Submerge z, with or without yaw
    waypoint.controller_type = cola2_msgs::WorldWaypointGoal::GOTO;
  }

  // Is it a keep position?
  waypoint.keep_position = req.keep_position;

  // Update captain status and set waypoint timeout
  waypoint.timeout = req.timeout;
  if (req.keep_position)
  {
    // Check timeout
    if (waypoint.timeout > 0.0)
    {
      ROS_INFO_STREAM("Keep position TRUE. Setting timeout to " << waypoint.timeout << " seconds");
    }
    else
    {
      ROS_INFO_STREAM("Keep position TRUE but the requested timeout is 0.0. Keeping position for 3600.0 seconds");
      waypoint.timeout = 3600.0;
    }
  }

  // Check altitude mode
  if (waypoint.altitude_mode)
  {
    ROS_INFO_STREAM("Send waypoint request at [" << waypoint.position.north << ", " << waypoint.position.east
                                                 << "] with altitude " << waypoint.altitude << ". Timeout is "
                                                 << waypoint.timeout << " seconds");
  }
  else
  {
    ROS_INFO_STREAM("Send waypoint request at [" << waypoint.position.north << ", " << waypoint.position.east
                                                 << "] with depth " << waypoint.position.depth << ". Timeout is "
                                                 << waypoint.timeout << " seconds");
  }

  // Call actionlib
  is_waypoint_actionlib_running_ = true;
  waypoint_actionlib_.sendGoal(waypoint);

  // Return success
  res.message = "Enabled";
  res.success = true;
  return true;
}

void Captain::goTo()
{
  // Feedback
  goto_feedback_.state = cola2_msgs::CaptainStateFeedback::ACTIVE;
  double timeout, distance;
  computeGotoTimeoutAndDistance(goto_req_, timeout, distance);
  goto_feedback_.time_to_finish = timeout;
  goto_feedback_.header.stamp = ros::Time::now();
  setKeyValue(goto_feedback_, "distance_to_end", std::to_string(distance));
  pub_captain_state_feedback_.publish(goto_feedback_);
}

bool Captain::gotoHasFinished()
{
  bool result = waypoint_actionlib_.waitForResult(ros::Duration(0.1));
  is_waypoint_actionlib_running_ = (!result);
  return result;
}

void Captain::computeSectionTimeoutAndDistance(cola2_msgs::Section::Request& req, double& timeout, double& distance)
{
  timeout = 0.0;
  distance = 0.0;

  // Check req.reference to transform req.position to appropiate reference frame
  double initial_x, initial_y, initial_z, final_x, final_y, final_z, min_x_vel, min_z_vel;
  if (req.reference == cola2_msgs::GotoRequest::REFERENCE_NED)
  {
    initial_x = req.initial_position.x;
    initial_y = req.initial_position.y;
    final_x = req.final_position.x;
    final_y = req.final_position.y;
  }
  else if (req.reference == cola2_msgs::GotoRequest::REFERENCE_GLOBAL)
  {
    // Convert waypoint data
    cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
    ned.geodetic2Ned(req.initial_position.x, req.initial_position.y, 0.0, initial_x, initial_y, initial_z);
    ned.geodetic2Ned(req.final_position.x, req.final_position.y, 0.0, final_x, final_y, final_z);
  }
  else
  {
    ROS_ERROR_STREAM("Invalid reference frame in computeSectionTimeoutAndDistance");
    return;
  }
  initial_z = req.initial_position.z;
  final_z = req.final_position.z;
  if (req.altitude_mode)
  {
    initial_z = nav_.z + nav_.altitude - req.initial_position.z;
    final_z = nav_.z + nav_.altitude - req.final_position.z;
  }

  // Compute min velocities TODO: This should depend on the chosen controller
  double max_x_vel, max_z_vel;
  cola2::ros::getParam(cola2::ros::getNamespace() + "/pilot/los_cte/max_surge_velocity", max_x_vel, 0.1);
  cola2::ros::getParam(cola2::ros::getNamespace() + "/controller/max_velocity_z", max_z_vel, 0.1);
  min_x_vel = std::min(req.surge_velocity, max_x_vel);
  min_z_vel = std::min(req.surge_velocity, max_z_vel);

  // Compute distances
  double dist_xy = std::sqrt(std::pow(initial_x - nav_.x, 2) + std::pow(initial_y - nav_.y, 2)) +
                   std::sqrt(std::pow(final_x - initial_x, 2) + std::pow(final_y - initial_y, 2));
  double dist_z = std::fabs(initial_z - nav_.z) + std::fabs(final_z - initial_z);

  // Limit min velocities
  if (min_x_vel < 1e-3)
    min_x_vel = 1e-3;
  if (min_z_vel < 1e-3)
    min_z_vel = 1e-3;

  // Compute timeout
  double time_xy = 2.0 * dist_xy / min_x_vel;
  double time_z = 2.0 * dist_z / min_z_vel;
  timeout = std::max(time_xy, time_z) + 30.0;
  distance = std::sqrt(std::pow(final_x - nav_.x, 2) + std::pow(final_y - nav_.y, 2));
}

bool Captain::enableSection(cola2_msgs::Section::Request& req, cola2_msgs::Section::Response& res)
{
  // Define section attributes
  cola2_msgs::WorldSectionGoal section;
  section.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
  if (req.controller_type == cola2_msgs::Section::Request::LOSCTE)
  {
    section.controller_type = cola2_msgs::WorldSectionGoal::LOSCTE;
  }
  else
  {
    std::string msg("Invalid section controller type");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }
  section.disable_z = req.disable_z;
  section.tolerance.x = req.tolerance.x;
  section.tolerance.y = req.tolerance.y;
  section.tolerance.z = req.tolerance.z;
  section.surge_velocity = req.surge_velocity;
  section.altitude_mode = req.altitude_mode;
  section.timeout = req.timeout;

  // Initial and final positions
  if (req.reference == cola2_msgs::Section::Request::REFERENCE_NED)
  {
    section.initial_position.x = req.initial_position.x;
    section.initial_position.y = req.initial_position.y;
    section.initial_position.z = req.initial_position.z;
    section.final_position.x = req.final_position.x;
    section.final_position.y = req.final_position.y;
    section.final_position.z = req.final_position.z;
  }
  else if (req.reference == cola2_msgs::Section::Request::REFERENCE_GLOBAL)
  {
    cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
    double north, east, depth;
    ned.geodetic2Ned(req.initial_position.x, req.initial_position.y, req.initial_position.z, north, east, depth);
    section.initial_position.x = north;
    section.initial_position.y = east;
    section.initial_position.z = req.initial_position.z;

    ned.geodetic2Ned(req.final_position.x, req.final_position.y, req.final_position.z, north, east, depth);
    section.final_position.x = north;
    section.final_position.y = east;
    section.final_position.z = req.final_position.z;
  }
  else
  {
    std::string msg("Invalid section reference frame");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }

  // Check safety distance
  double distance_to_initial_position =
      std::sqrt(std::pow(section.initial_position.x - nav_.x, 2) + std::pow(section.initial_position.y - nav_.y, 2));
  double section_length = std::sqrt(std::pow(section.initial_position.x - section.final_position.x, 2) +
                                    std::pow(section.initial_position.y - section.final_position.y, 2));
  double safety_distance = std::max(distance_to_initial_position, section_length);

  // Load max distance to waypoint from param server and check request
  double max_distance_to_waypoint;  // TODO: change the name of the parameter
  cola2::ros::getParam("~max_distance_to_waypoint", max_distance_to_waypoint, 300.0);  // Default value
  if (safety_distance > max_distance_to_waypoint)
  {
    std::stringstream msg_ss;
    msg_ss << "Invalid request. Max distance to section is " << max_distance_to_waypoint
           << " while requested section distance is at " << safety_distance;
    ROS_ERROR_STREAM(msg_ss.str());
    res.message = msg_ss.str();
    res.success = false;
    return false;
  }

  if (section.altitude_mode)
  {
    ROS_INFO_STREAM("Send section request from ["
                    << section.initial_position.x << ", " << section.initial_position.y << "] to ["
                    << section.final_position.x << ", " << section.final_position.y << "] with altitude from "
                    << section.initial_position.z << " to " << section.final_position.z << ". Timeout is "
                    << section.timeout << " seconds");
  }
  else
  {
    ROS_INFO_STREAM("Send section request from ["
                    << section.initial_position.x << ", " << section.initial_position.y << "] to ["
                    << section.final_position.x << ", " << section.final_position.y << "] with depth from "
                    << section.initial_position.z << " to " << section.final_position.z << ". Timeout is "
                    << section.timeout << " seconds");
  }

  // Send section using actionlib
  is_section_actionlib_running_ = true;
  section_actionlib_.sendGoal(section);

  // Return success
  res.message = "Enabled";
  res.success = true;
  return true;
}

void Captain::section()
{
  // Feedback
  section_feedback_.state = cola2_msgs::CaptainStateFeedback::ACTIVE;
  double timeout, distance;
  computeSectionTimeoutAndDistance(section_req_, timeout, distance);
  section_feedback_.time_to_finish = timeout;
  section_feedback_.header.stamp = ros::Time::now();
  setKeyValue(section_feedback_, "distance_to_end", std::to_string(distance));
  pub_captain_state_feedback_.publish(section_feedback_);
}

bool Captain::sectionHasFinished()
{
  bool result = section_actionlib_.waitForResult(ros::Duration(0.1));
  is_section_actionlib_running_ = (!result);
  return result;
}

std::vector<double> Captain::computeMissionTimeouts(Mission mission, std::size_t first_step)
{
  std::vector<double> timeouts;
  double max_x_vel_goto, max_x_vel_section, max_z_vel;
  cola2::ros::getParam(cola2::ros::getNamespace() + "/pilot/goto/max_surge", max_x_vel_goto, 0.1);
  cola2::ros::getParam(cola2::ros::getNamespace() + "/pilot/los_cte/max_surge_velocity", max_x_vel_section, 0.1);
  cola2::ros::getParam(cola2::ros::getNamespace() + "/controller/max_velocity_z", max_z_vel, 0.1);
  cola2::utils::NED ned(last_ned_lat_origin_, last_ned_lon_origin_, 0.0);
  double previous_x = nav_.x;
  double previous_y = nav_.y;
  double previous_z = nav_.z;
  for (std::size_t i = first_step; i < mission.size(); ++i)
  {
    auto step = mission.getStep(i);

    double initial_x, initial_y, initial_z, final_x, final_y, final_z, min_x_vel, min_z_vel, wait_time;

    if (step->getManeuverPtr()->getManeuverType() == WAYPOINT_MANEUVER)  // This comes from mission_maneuver.h
    {
      auto maneuver_wp = std::dynamic_pointer_cast<MissionWaypoint>(step->getManeuverPtr());

      // Get final position
      ned.geodetic2Ned(maneuver_wp->getPosition().getLatitude(), maneuver_wp->getPosition().getLongitude(), 0.0,
                       final_x, final_y, final_z);
      final_z = maneuver_wp->getPosition().getZ();
      if (maneuver_wp->getPosition().getAltitudeMode())
      {
        final_z = nav_.z + nav_.altitude - maneuver_wp->getPosition().getZ();
      }
      initial_x = final_x;
      initial_y = final_y;
      initial_z = final_z;

      // Compute min velocities
      min_x_vel = std::min(maneuver_wp->getSpeed(), max_x_vel_goto);
      min_z_vel = std::min(maneuver_wp->getSpeed(), max_z_vel);

      // Wait time
      wait_time = 0.0;
    }
    else if (step->getManeuverPtr()->getManeuverType() == SECTION_MANEUVER)
    {
      auto maneuver_sec = std::dynamic_pointer_cast<MissionSection>(step->getManeuverPtr());

      // Get initial and final position
      ned.geodetic2Ned(maneuver_sec->getInitialPosition().getLatitude(),
                       maneuver_sec->getInitialPosition().getLongitude(), 0.0, initial_x, initial_y, initial_z);
      initial_z = maneuver_sec->getInitialPosition().getZ();
      if (maneuver_sec->getInitialPosition().getAltitudeMode())
      {
        initial_z = nav_.z + nav_.altitude - maneuver_sec->getInitialPosition().getZ();
      }
      ned.geodetic2Ned(maneuver_sec->getFinalPosition().getLatitude(), maneuver_sec->getFinalPosition().getLongitude(),
                       0.0, final_x, final_y, final_z);
      final_z = maneuver_sec->getFinalPosition().getZ();
      if (maneuver_sec->getFinalPosition().getAltitudeMode())
      {
        final_z = nav_.z + nav_.altitude - maneuver_sec->getFinalPosition().getZ();
      }

      // Compute min velocities
      min_x_vel = std::min(maneuver_sec->getSpeed(), max_x_vel_section);
      min_z_vel = std::min(maneuver_sec->getSpeed(), max_z_vel);

      // Wait time
      wait_time = 0.0;
    }
    else if (step->getManeuverPtr()->getManeuverType() == PARK_MANEUVER)
    {
      auto maneuver_park = std::dynamic_pointer_cast<MissionPark>(step->getManeuverPtr());

      // Get final position
      ned.geodetic2Ned(maneuver_park->getPosition().getLatitude(), maneuver_park->getPosition().getLongitude(), 0.0,
                       final_x, final_y, final_z);
      final_z = maneuver_park->getPosition().getZ();
      if (maneuver_park->getPosition().getAltitudeMode())
      {
        final_z = nav_.z + nav_.altitude - maneuver_park->getPosition().getZ();
      }
      initial_x = final_x;
      initial_y = final_y;
      initial_z = final_z;

      // Compute min velocities
      min_x_vel = std::min(0.3, max_x_vel_goto);  // Hardcoded park velocity
      min_z_vel = std::min(0.3, max_z_vel);

      // Wait time
      wait_time = static_cast<double>(maneuver_park->getTime());
    }
    else
    {
      ROS_ERROR_STREAM("Unknown maneuver type");
      timeouts.push_back(0.0);
      continue;
    }

    // Compute distances
    double dist_xy = std::sqrt(std::pow(initial_x - previous_x, 2) + std::pow(initial_y - previous_y, 2)) +
                     std::sqrt(std::pow(final_x - initial_x, 2) + std::pow(final_y - initial_y, 2));
    double dist_z = std::fabs(initial_z - previous_z) + std::fabs(final_z - initial_z);

    // Limit min velocities
    if (min_x_vel < 1e-3)
      min_x_vel = 1e-3;
    if (min_z_vel < 1e-3)
      min_z_vel = 1e-3;

    // Compute timeout
    double time_xy = 2.0 * dist_xy / min_x_vel;
    double time_z = 2.0 * dist_z / min_z_vel;
    double timeout = std::max(time_xy, time_z) + wait_time + 30.0;
    timeouts.push_back(timeout);

    // Store position
    previous_x = final_x;
    previous_y = final_y;
    previous_z = final_z;
  }

  return timeouts;
}

bool Captain::enableMission(cola2_msgs::Mission::Request& req, cola2_msgs::Mission::Response& res)
{
  Mission mission;

  // Get path were missions are stored
  std::string package;
  if (!cola2::ros::getParam("~vehicle_config_launch_mission_package", package))
  {
    std::string msg("Package vehicle_config_launch_mission not defined!");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }
  std::string package_path = ros::package::getPath(package);
  if (package_path.empty())
  {
    std::string msg("Error defining mission path!");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }
  std::string mission_path = package_path + "/missions/" + req.mission;

  // Load mission
  try
  {
    ROS_INFO_STREAM("Loading mission: " << mission_path);
    mission.loadMission(mission_path);
    ROS_INFO_STREAM("Mission loaded");
  }
  catch (const std::exception& ex)
  {
    std::string msg("Problem loading mission: ");
    msg += ex.what();
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }

  // Check services in the mission before starting
  std::set<std::string> missing_services;  // Use set to avoid reporting duplicates
  for (std::size_t i = 0; i < mission.size(); ++i)
  {
    std::vector<MissionAction> actions = mission.getStep(i)->getActions();
    for (const auto& action : actions)
    {
      if (ros::isShuttingDown())
      {
        return true;
      }
      std::string action_id = action.getActionId();
      if (missing_services.find(action_id) != missing_services.end())
      {
        continue;  // Already in missing_services
      }
      if (action.getIsEmpty())
      {
        ros::ServiceClient action_client = nh_.serviceClient<std_srvs::Trigger>(action_id);
        if (!action_client.waitForExistence(ros::Duration(1.0)))
        {
          missing_services.insert(action_id);
        }
      }
      else
      {
        ros::ServiceClient action_client = nh_.serviceClient<cola2_msgs::Action>(action_id);
        if (!action_client.waitForExistence(ros::Duration(1.0)))
        {
          missing_services.insert(action_id);
        }
      }
    }
  }
  if (!missing_services.empty())
  {
    std::string msg("Problem loading mission. Missing services:");
    for (const auto& service : missing_services)
    {
      msg += " " + service;
    }
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return false;
  }

  // Add it to the loaded missions
  MissionWithState mission_with_state;
  mission_with_state.mission = mission;
  mission_with_state.last_active = ros::Time::now();
  mission_with_state.step_timeouts = computeMissionTimeouts(mission, 0);
  loaded_missions_.insert({ req.mission, mission_with_state });

  // Return success
  res.message = "Enabled";
  res.success = true;
  return true;
}

void Captain::mission()
{
  // Create reference to mission
  Mission& mission = loaded_missions_[last_running_mission_].mission;

  // Feedback
  mission_feedback_.state = cola2_msgs::CaptainStateFeedback::ACTIVE;
  std::vector<double> timeouts = computeMissionTimeouts(mission, loaded_missions_[last_running_mission_].current_step);
  mission_feedback_.time_to_finish = std::accumulate(timeouts.begin(), timeouts.end(), 0.0);
  setKeyValue(mission_feedback_, "current_step",
              std::to_string(loaded_missions_[last_running_mission_].current_step + 1));
  setKeyValue(mission_feedback_, "total_steps", std::to_string(mission.size()));

  // Publish mission path
  nav_msgs::Path path = createPathFromMission(mission);
  pub_path_.publish(path);

  // Main loop over mission steps
  if (loaded_missions_[last_running_mission_].current_step < mission.size())
  {
    // Update last active time
    loaded_missions_[last_running_mission_].last_active = ros::Time::now();

    // Get step pointer
    auto step = mission.getStep(loaded_missions_[last_running_mission_].current_step);

    // Play mission step maneuver
    if (step->getManeuverPtr()->getManeuverType() == WAYPOINT_MANEUVER)  // This comes from mission_maneuver.h
    {
      auto maneuver_wp = std::dynamic_pointer_cast<MissionWaypoint>(step->getManeuverPtr());

      // Feedback
      setKeyValue(mission_feedback_, "active_controller", "Waypoint");
      setKeyValue(mission_feedback_, "altitude_mode",
                  (maneuver_wp->getPosition().getAltitudeMode() ? "true" : "false"));

      if (!waypoint_enabled_)
      {
        ROS_INFO_STREAM("Executing mission waypoint. Step " << loaded_missions_[last_running_mission_].current_step);
        waypoint_enabled_ = true;
        cola2_msgs::Goto::Request goto_req;
        cola2_msgs::Goto::Response goto_res;
        goto_req.altitude = maneuver_wp->getPosition().getZ();
        goto_req.altitude_mode = maneuver_wp->getPosition().getAltitudeMode();
        goto_req.linear_velocity.x = maneuver_wp->getSpeed();
        goto_req.position.x = maneuver_wp->getPosition().getLatitude();
        goto_req.position.y = maneuver_wp->getPosition().getLongitude();
        goto_req.position.z = maneuver_wp->getPosition().getZ();
        goto_req.position_tolerance.x = maneuver_wp->getTolerance().getX();
        goto_req.position_tolerance.y = maneuver_wp->getTolerance().getY();
        goto_req.position_tolerance.z = maneuver_wp->getTolerance().getZ();
        goto_req.blocking = false;
        goto_req.keep_position = false;
        goto_req.disable_axis.x = false;
        goto_req.disable_axis.y = true;
        goto_req.disable_axis.z = false;
        goto_req.disable_axis.roll = true;
        goto_req.disable_axis.yaw = false;
        goto_req.disable_axis.pitch = true;
        goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
        goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_GLOBAL;
        goto_req.timeout =
            loaded_missions_[last_running_mission_].step_timeouts[loaded_missions_[last_running_mission_].current_step];
        if (!enableGoto(goto_req, goto_res))
        {
          ROS_ERROR_STREAM("Enable waypoint failed");
          // waypoint_enabled_ = false;
          // step_completed_ = true;
          disableMissionHelper();
          return;
        }
      }
      if (waypoint_enabled_ && gotoHasFinished())
      {
        ROS_INFO_STREAM("Mission waypoint finalized");
        waypoint_enabled_ = false;
        step_completed_ = true;
      }
    }
    else if (step->getManeuverPtr()->getManeuverType() == SECTION_MANEUVER)
    {
      auto maneuver_sec = std::dynamic_pointer_cast<MissionSection>(step->getManeuverPtr());

      // Feedback
      setKeyValue(mission_feedback_, "active_controller", "Section");
      setKeyValue(mission_feedback_, "altitude_mode",
                  (maneuver_sec->getInitialPosition().getAltitudeMode() ? "true" : "false"));

      if (!section_enabled_)
      {
        ROS_INFO_STREAM("Execute mission section. Step " << loaded_missions_[last_running_mission_].current_step);
        section_enabled_ = true;
        cola2_msgs::Section::Request section_req;
        cola2_msgs::Section::Response section_res;
        section_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
        section_req.controller_type = cola2_msgs::Section::Request::LOSCTE;
        section_req.reference = cola2_msgs::Section::Request::REFERENCE_GLOBAL;
        section_req.disable_z = false;
        section_req.tolerance.x = maneuver_sec->getTolerance().getX();
        section_req.tolerance.y = maneuver_sec->getTolerance().getY();
        section_req.tolerance.z = maneuver_sec->getTolerance().getZ();
        section_req.surge_velocity = maneuver_sec->getSpeed();
        section_req.initial_position.x = maneuver_sec->getInitialPosition().getLatitude();
        section_req.initial_position.y = maneuver_sec->getInitialPosition().getLongitude();
        section_req.initial_position.z = maneuver_sec->getInitialPosition().getZ();
        section_req.final_position.x = maneuver_sec->getFinalPosition().getLatitude();
        section_req.final_position.y = maneuver_sec->getFinalPosition().getLongitude();
        section_req.final_position.z = maneuver_sec->getFinalPosition().getZ();
        section_req.altitude_mode = maneuver_sec->getInitialPosition().getAltitudeMode();
        section_req.timeout =
            loaded_missions_[last_running_mission_].step_timeouts[loaded_missions_[last_running_mission_].current_step];
        if (!enableSection(section_req, section_res))
        {
          ROS_ERROR_STREAM("Enable section failed");
          // section_enabled_ = false;
          // step_completed_ = true;
          disableMissionHelper();
          return;
        }
      }
      if (section_enabled_ && sectionHasFinished())
      {
        ROS_INFO_STREAM("Mission section finalized");
        section_enabled_ = false;
        step_completed_ = true;
      }
    }
    else if (step->getManeuverPtr()->getManeuverType() == PARK_MANEUVER)
    {
      auto maneuver_park = std::dynamic_pointer_cast<MissionPark>(step->getManeuverPtr());

      // Feedback
      setKeyValue(mission_feedback_, "active_controller", "Park");
      setKeyValue(mission_feedback_, "altitude_mode",
                  (maneuver_park->getPosition().getAltitudeMode() ? "true" : "false"));

      if (!approach_completed_)
      {
        // Approach
        if (!waypoint_enabled_)
        {
          ROS_INFO_STREAM("Executing mission park approach. Step "
                          << loaded_missions_[last_running_mission_].current_step);
          waypoint_enabled_ = true;
          cola2_msgs::Goto::Request goto_req;
          cola2_msgs::Goto::Response goto_res;
          goto_req.altitude = static_cast<float>(maneuver_park->getPosition().getZ());
          goto_req.altitude_mode = maneuver_park->getPosition().getAltitudeMode();
          goto_req.linear_velocity.x = 0.3;  // Fixed velocity when reaching park waypoint
          goto_req.position.x = maneuver_park->getPosition().getLatitude();
          goto_req.position.y = maneuver_park->getPosition().getLongitude();
          goto_req.position.z = maneuver_park->getPosition().getZ();
          goto_req.position_tolerance.x = 3.0;
          goto_req.position_tolerance.y = 3.0;
          goto_req.position_tolerance.z = 1.5;
          goto_req.blocking = false;
          goto_req.keep_position = false;
          goto_req.disable_axis.x = false;
          goto_req.disable_axis.y = true;
          goto_req.disable_axis.z = false;
          goto_req.disable_axis.roll = true;
          goto_req.disable_axis.yaw = false;
          goto_req.disable_axis.pitch = true;
          goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
          goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_GLOBAL;
          goto_req.timeout = loaded_missions_[last_running_mission_]
                                 .step_timeouts[loaded_missions_[last_running_mission_].current_step] -
                             static_cast<double>(maneuver_park->getTime());
          if (!enableGoto(goto_req, goto_res))
          {
            ROS_ERROR_STREAM("Enable park approach failed");
            // waypoint_enabled_ = false;
            // approach_completed_ = true;
            disableMissionHelper();
            return;
          }
        }
        if (waypoint_enabled_ && gotoHasFinished())
        {
          ROS_INFO_STREAM("Mission park approach finalized");
          waypoint_enabled_ = false;
          approach_completed_ = true;
        }
      }
      else
      {
        // Keep position
        if (!waypoint_enabled_)
        {
          ROS_INFO_STREAM("Executing mission park wait for " << maneuver_park->getTime() << " seconds. Step "
                                                             << loaded_missions_[last_running_mission_].current_step);
          waypoint_enabled_ = true;
          cola2_msgs::Goto::Request goto_req;
          cola2_msgs::Goto::Response goto_res;
          goto_req.altitude = static_cast<float>(maneuver_park->getPosition().getZ());
          goto_req.altitude_mode = maneuver_park->getPosition().getAltitudeMode();
          goto_req.linear_velocity.x = 0.3;  // Fixed velocity when reaching park waypoint
          goto_req.position.x = maneuver_park->getPosition().getLatitude();
          goto_req.position.y = maneuver_park->getPosition().getLongitude();
          goto_req.position.z = maneuver_park->getPosition().getZ();
          goto_req.position_tolerance.x = 0.0;
          goto_req.position_tolerance.y = 0.0;
          goto_req.position_tolerance.z = 0.0;
          goto_req.blocking = false;
          goto_req.keep_position = true;
          goto_req.disable_axis.x = false;
          goto_req.disable_axis.y = true;
          goto_req.disable_axis.z = false;
          goto_req.disable_axis.roll = true;
          goto_req.disable_axis.yaw = false;
          goto_req.disable_axis.pitch = true;
          goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
          goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_GLOBAL;
          goto_req.timeout = static_cast<double>(maneuver_park->getTime());
          if (!enableGoto(goto_req, goto_res))
          {
            ROS_ERROR_STREAM("Enable park keep position failed");
            // waypoint_enabled_ = false;
            // approach_completed_ = false;
            // step_completed_ = true;
            disableMissionHelper();
            return;
          }
        }
        if (waypoint_enabled_ && gotoHasFinished())
        {
          ROS_INFO_STREAM("Mission park wait finalized");
          waypoint_enabled_ = false;
          approach_completed_ = false;
          step_completed_ = true;
        }
      }
    }

    // Publish feedback
    mission_feedback_.header.stamp = ros::Time::now();
    pub_captain_state_feedback_.publish(mission_feedback_);

    // Check if the step maneuvers are done
    if (step_completed_)
    {
      // Call actions
      std::vector<MissionAction> actions = step->getActions();
      for (const auto& action : actions)
      {
        if (ros::isShuttingDown())
        {
          return;
        }
        callAction(action.getIsEmpty(), action.getActionId(), action.getParameters());
        /*ros::Duration(2.0).sleep();*/
      }

      // Increment step
      ++loaded_missions_[last_running_mission_].current_step;
      step_completed_ = false;
    }
  }
}

bool Captain::missionHasFinished()
{
  return (loaded_missions_[last_running_mission_].current_step >=
          loaded_missions_[last_running_mission_].mission.size());
}

void Captain::keepPosition()
{
  // Feedback
  keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::ACTIVE;
  keep_position_feedback_.time_to_finish =
      last_keep_position_duration_ - (ros::Time::now().toSec() - last_keep_position_time_);
  keep_position_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(keep_position_feedback_);
}

void Captain::safetyKeepPosition()
{
  // Feedback
  safety_keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::ACTIVE;
  safety_keep_position_feedback_.time_to_finish =
      last_keep_position_duration_ - (ros::Time::now().toSec() - last_keep_position_time_);
  safety_keep_position_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(safety_keep_position_feedback_);
}

bool Captain::enableGotoSrv(cola2_msgs::Goto::Request& req, cola2_msgs::Goto::Response& res)
{
  // Show deprecated message
  if (req.blocking)
  {
    ROS_ERROR_STREAM("Received deprecated blocking goto. Captain is now nonblocking. Use helpers in cola2_lib_ros "
                     "instead");
  }

  // Check current state
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable goto. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Enable goto
  double timeout, distance;
  computeGotoTimeoutAndDistance(req, timeout, distance);
  if (timeout < req.timeout)
  {
    req.timeout = timeout;
  }
  goto_req_ = req;
  if (enableGoto(req, res))
  {
    ROS_INFO_STREAM("Goto enabled");
    state_ = CaptainStates::Goto;
  }

  mainIteration();
  return true;
}

bool Captain::disableGotoSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Goto)
  {
    std::string msg("Impossible to disable goto. Captain not in goto state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Reset flags
  resetManeuverFlags();

  // Cancel actionlib
  cancelWaypointActionlib();

  // Set captain state
  state_ = CaptainStates::Idle;

  // Feedback
  goto_feedback_.state = cola2_msgs::CaptainStateFeedback::STOPPED;
  goto_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(goto_feedback_);
  ++goto_feedback_.id;

  res.message = "Goto disabled";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

bool Captain::enableSectionSrv(cola2_msgs::Section::Request& req, cola2_msgs::Section::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable section. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Enable section
  double timeout, distance;
  computeSectionTimeoutAndDistance(req, timeout, distance);
  if (timeout < req.timeout)
  {
    req.timeout = timeout;
  }
  section_req_ = req;
  if (enableSection(req, res))
  {
    ROS_INFO_STREAM("Section enabled");
    state_ = CaptainStates::Section;
  }

  mainIteration();
  return true;
}

bool Captain::disableSectionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Section)
  {
    std::string msg("Impossible to disable section. Captain not in section state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Reset flags
  resetManeuverFlags();

  // Cancel actionlib
  cancelSectionActionlib();

  // Set captain state
  state_ = CaptainStates::Idle;

  // Feedback
  section_feedback_.state = cola2_msgs::CaptainStateFeedback::STOPPED;
  section_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(section_feedback_);
  ++section_feedback_.id;

  res.message = "Section disabled";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

bool Captain::enableMissionSrv(cola2_msgs::Mission::Request& req, cola2_msgs::Mission::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable mission. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Check mission name. If empty, use default mission name
  if (req.mission.empty())
  {
    req.mission = getDefaultMissionName();
    if (req.mission.empty())
    {
      std::string msg("Impossible to obtain default mission name");
      ROS_ERROR_STREAM(msg);
      res.message = msg;
      res.success = false;
      return true;
    }
    ROS_INFO_STREAM("Enabling mission without name. Using " << req.mission);
  }

  // Remove old mission with the same name
  deleteLoadedMission(req.mission);

  // Enable mission
  if (enableMission(req, res))
  {
    ROS_INFO_STREAM("Mission enabled");
    state_ = CaptainStates::Mission;
    last_running_mission_ = req.mission;
  }

  mainIteration();
  return true;
}

bool Captain::resumeMissionSrv(cola2_msgs::Mission::Request& req, cola2_msgs::Mission::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to resume mission. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Check mission name. If empty, use default last mission name
  if (req.mission.empty())
  {
    req.mission = getDefaultMissionName();
    if (req.mission.empty())
    {
      std::string msg("Impossible to obtain default mission name");
      ROS_ERROR_STREAM(msg);
      res.message = msg;
      res.success = false;
      return true;
    }
    ROS_INFO_STREAM("Resuming mission without name. Using " << req.mission);
  }

  // If the mission was not found, display a message and return
  if (loaded_missions_.find(req.mission) == loaded_missions_.end())
  {
    std::string msg("Impossible to resume ");
    msg += req.mission + ". This mission was disabled or has never been enabled";
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Set captain state
  state_ = CaptainStates::Mission;
  last_running_mission_ = req.mission;

  res.message = "Resuming mission";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  mainIteration();
  return true;
}

void Captain::resetManeuverFlags()
{
  waypoint_enabled_ = false;
  section_enabled_ = false;
  approach_completed_ = false;
}

bool Captain::deleteLoadedMission(const std::string& mission_name)
{
  if (loaded_missions_.find(mission_name) != loaded_missions_.end())
  {
    loaded_missions_.erase(loaded_missions_.find(mission_name));
    return true;
  }
  return false;
}

bool Captain::pauseMissionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::Mission)
  {
    std::string msg("Impossible to pause mission. Captain not in mission state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Reset flags
  resetManeuverFlags();

  // Cancel actionlibs if necessary
  cancelWaypointActionlib();
  cancelSectionActionlib();

  // Change state
  state_ = CaptainStates::Idle;

  res.message = "Mission paused";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

void Captain::disableMissionHelper()
{
  // Cancel actionlibs if necessary
  cancelWaypointActionlib();
  cancelSectionActionlib();

  // Execute pending actions
  executePendingActions(last_running_mission_);

  // Reset flags
  resetManeuverFlags();

  // Remove from loaded missions
  deleteLoadedMission(last_running_mission_);

  // Set captain state
  state_ = CaptainStates::Idle;

  // Feedback
  mission_feedback_.state = cola2_msgs::CaptainStateFeedback::STOPPED;
  mission_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(mission_feedback_);
  ++mission_feedback_.id;
}

bool Captain::disableMissionSrv(cola2_msgs::Mission::Request& req, cola2_msgs::Mission::Response& res)
{
  if ((state_ == CaptainStates::Mission) && (req.mission.empty() || (req.mission == last_running_mission_)))
  {
    // Disable mission
    disableMissionHelper();

    res.message = "Active mission disabled: " + last_running_mission_;
    res.success = true;
    ROS_INFO_STREAM(res.message);
    return true;
  }
  else
  {
    // Check name
    if (req.mission.empty())
    {
      req.mission = getDefaultMissionName();
    }

    // If found, execute pending actions and remove from loaded missions
    std::string msg;
    if (loaded_missions_.find(req.mission) != loaded_missions_.end())
    {
      executePendingActions(req.mission);
      deleteLoadedMission(req.mission);
      msg = "Mission " + req.mission + " removed from paused missions";
      ROS_INFO_STREAM(msg);
    }
    else
    {
      msg = "Mission not found. Nothing to remove";
      ROS_WARN_STREAM(msg);
    }
    res.message = msg;
    res.success = true;
    return true;
  }
}

bool Captain::enableKeepPositionHolonomicSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ == CaptainStates::KeepPosition)
  {
    std::string msg("Keep position already enabled");
    ROS_INFO_STREAM(msg);
    res.message = msg;
    res.success = true;
    return true;
  }
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable keep position. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Call goto
  cola2_msgs::Goto::Request goto_req;
  cola2_msgs::Goto::Response goto_res;
  goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
  goto_req.altitude_mode = false;
  goto_req.blocking = false;
  goto_req.keep_position = true;
  goto_req.disable_axis.x = false;
  goto_req.disable_axis.y = false;
  goto_req.disable_axis.z = false;
  goto_req.disable_axis.roll = true;
  goto_req.disable_axis.pitch = true;
  goto_req.disable_axis.yaw = false;
  goto_req.position.x = nav_.x;
  goto_req.position.y = nav_.y;
  goto_req.position.z = nav_.z;
  goto_req.yaw = static_cast<float>(nav_.yaw);
  goto_req.position_tolerance.x = 0.0;  // If tolerance is 0.0 position, the waypoint is impossible to reach
  goto_req.position_tolerance.y = 0.0;  // and therefore the controller will never finish
  goto_req.position_tolerance.z = 0.0;
  goto_req.orientation_tolerance.yaw = 0.0;
  goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_NED;
  goto_req.timeout = 3600.0;
  if (enableGoto(goto_req, goto_res))
  {
    state_ = CaptainStates::KeepPosition;
    last_keep_position_holonomic_ = true;
    last_keep_position_time_ = ros::Time::now().toSec();
    last_keep_position_duration_ = goto_req.timeout;
  }
  else
  {
    res.message = goto_res.message;
    res.success = goto_res.success;
    return true;
  }

  res.message = "Holonomic keep position enabled";
  res.success = true;
  ROS_INFO_STREAM("Start holonomic keep position at [" << nav_.x << ", " << nav_.y << ", " << nav_.z
                                                       << "] with orientation " << nav_.yaw);
  mainIteration();
  return true;
}

bool Captain::enableKeepPositionNonHolonomicSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ == CaptainStates::KeepPosition)
  {
    std::string msg("Keep position already enabled");
    ROS_INFO_STREAM(msg);
    res.message = msg;
    res.success = true;
    return true;
  }
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable keep position. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Call goto
  cola2_msgs::Goto::Request goto_req;
  cola2_msgs::Goto::Response goto_res;
  goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_NORMAL;
  goto_req.altitude_mode = false;
  goto_req.blocking = false;
  goto_req.keep_position = true;
  goto_req.disable_axis.x = false;
  goto_req.disable_axis.y = true;
  goto_req.disable_axis.z = false;
  goto_req.disable_axis.roll = true;
  goto_req.disable_axis.pitch = true;
  goto_req.disable_axis.yaw = false;
  goto_req.position.x = nav_.x;
  goto_req.position.y = nav_.y;
  goto_req.position.z = nav_.z;
  goto_req.yaw = static_cast<float>(nav_.yaw);
  goto_req.position_tolerance.x = 0.0;  // If tolerance is 0.0 position, the waypoint is impossible to reach
  goto_req.position_tolerance.y = 0.0;  // and therefore the controller will never finish
  goto_req.position_tolerance.z = 0.0;
  goto_req.orientation_tolerance.yaw = 0.0;
  goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_NED;
  goto_req.timeout = 3600.0;
  if (enableGoto(goto_req, goto_res))
  {
    state_ = CaptainStates::KeepPosition;
    last_keep_position_holonomic_ = false;
    last_keep_position_time_ = ros::Time::now().toSec();
    last_keep_position_duration_ = goto_req.timeout;
  }
  else
  {
    res.message = goto_res.message;
    res.success = goto_res.success;
    return true;
  }

  res.message = "Nonholonomic keep position enabled";
  res.success = true;
  ROS_INFO_STREAM("Start nonholonomic keep position at [" << nav_.x << ", " << nav_.y << ", " << nav_.z
                                                          << "] with orientation " << nav_.yaw);
  mainIteration();
  return true;
}

bool Captain::disableKeepPositionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::KeepPosition)
  {
    std::string msg("Impossible to disable keep position. Captain not in keep position state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Reset flags
  resetManeuverFlags();

  // Set captain state
  state_ = CaptainStates::Idle;

  // Cancel actionlib if necessary
  cancelWaypointActionlib();

  // Set captain state
  state_ = CaptainStates::Idle;

  // Feedback
  keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::STOPPED;
  keep_position_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(keep_position_feedback_);
  ++keep_position_feedback_.id;

  res.message = "Keep position disabled";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

bool Captain::enableSafetyKeepPositionSrv(
    ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>& event)
{
  // Check current state
  if (state_ == CaptainStates::SafetyKeepPosition)
  {
    std::string msg("Safety keep position already enabled");
    ROS_INFO_STREAM(msg);
    event.getResponse().message = msg;
    event.getResponse().success = true;
    return true;
  }

  // Disable everything
  disableAllAndSetIdleSrv(event);

  // Get safety depth
  double controlled_surface_depth;
  cola2::ros::getParam("~controlled_surface_depth", controlled_surface_depth, 0.0);  // Default value

  // Call goto
  cola2_msgs::Goto::Request goto_req;
  cola2_msgs::Goto::Response goto_res;
  goto_req.priority = cola2_msgs::GoalDescriptor::PRIORITY_SAFETY;
  goto_req.altitude_mode = false;
  goto_req.blocking = false;
  goto_req.keep_position = true;
  goto_req.disable_axis.x = false;
  goto_req.disable_axis.y = true;
  goto_req.disable_axis.z = false;
  goto_req.disable_axis.roll = true;
  goto_req.disable_axis.pitch = true;
  goto_req.disable_axis.yaw = false;
  goto_req.position.x = nav_.x;
  goto_req.position.y = nav_.y;
  goto_req.position.z = controlled_surface_depth;
  goto_req.yaw = static_cast<float>(nav_.yaw);
  goto_req.position_tolerance.x = 0.0;  // If tolerance is 0.0 position, the waypoint is impossible to reach
  goto_req.position_tolerance.y = 0.0;  // and therefore the controller will never finish
  goto_req.position_tolerance.z = 0.0;
  goto_req.orientation_tolerance.yaw = 0.0;
  goto_req.reference = cola2_msgs::Goto::Request::REFERENCE_NED;
  goto_req.timeout = 1e6;  // Effectively waiting forever
  if (enableGoto(goto_req, goto_res))
  {
    state_ = CaptainStates::SafetyKeepPosition;
    last_keep_position_holonomic_ = false;
    last_keep_position_time_ = ros::Time::now().toSec();
    last_keep_position_duration_ = goto_req.timeout;
  }
  else
  {
    event.getResponse().message = goto_res.message;
    event.getResponse().success = goto_res.success;
    return true;
  }

  event.getResponse().message = "Safety nonholonomic keep position enabled";
  event.getResponse().success = true;
  ROS_INFO_STREAM("Start nonholonomic safety keep position at ["
                  << nav_.x << ", " << nav_.y << ", " << controlled_surface_depth << "] with orientation " << nav_.yaw);
  mainIteration();
  return true;
}

bool Captain::disableSafetyKeepPositionSrv(std_srvs::Trigger::Request&, std_srvs::Trigger::Response& res)
{
  // Check current state
  if (state_ != CaptainStates::SafetyKeepPosition)
  {
    std::string msg("Impossible to disable safety keep position. Captain not in safety keep position state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Reset flags
  resetManeuverFlags();

  // Cancel actionlib if necessary
  cancelWaypointActionlib();

  // Set captain state
  state_ = CaptainStates::Idle;

  // Feedback
  safety_keep_position_feedback_.state = cola2_msgs::CaptainStateFeedback::STOPPED;
  safety_keep_position_feedback_.header.stamp = ros::Time::now();
  pub_captain_state_feedback_.publish(safety_keep_position_feedback_);
  ++safety_keep_position_feedback_.id;

  res.message = "Safety keep position disabled";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

bool Captain::disableAllKeepPositionsSrv(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res)
{
  if (state_ == CaptainStates::KeepPosition)
  {
    disableKeepPositionSrv(req, res);
  }
  else if (state_ == CaptainStates::SafetyKeepPosition)
  {
    disableSafetyKeepPositionSrv(req, res);
  }
  else if (state_ == CaptainStates::Idle)
  {
    std::string msg("Already in Idle state");
    ROS_INFO_STREAM(msg);
    res.message = msg;
    res.success = true;
  }
  else
  {
    std::string msg("Impossible to disable keep positions. Captain is not Idle nor keeping position");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
  }
  return true;
}

bool Captain::resetKeepPositionSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>& event)
{
  double last_keep_position_time = last_keep_position_time_;
  std_srvs::Trigger::Request req = event.getRequest();
  if (state_ == CaptainStates::KeepPosition)
  {
    ROS_INFO_STREAM("Resetting keep position");
    disableKeepPositionSrv(req, event.getResponse());
    if (last_keep_position_holonomic_)
    {
      enableKeepPositionHolonomicSrv(req, event.getResponse());
    }
    else
    {
      enableKeepPositionNonHolonomicSrv(req, event.getResponse());
    }
  }
  else if (state_ == CaptainStates::SafetyKeepPosition)
  {
    ROS_INFO_STREAM("Resetting safety keep position");
    disableSafetyKeepPositionSrv(req, event.getResponse());
    enableSafetyKeepPositionSrv(event);
  }
  else
  {
    event.getResponse().success = true;
    event.getResponse().message = "Captain not in any keep position state. Reset does nothing";
  }
  last_keep_position_time_ = last_keep_position_time;
  return true;
}

bool Captain::enableExternalMissionSrv(
    ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>& event)
{
  // Get service response
  std_srvs::Trigger::Response& res = event.getResponse();

  // Check current state
  if (state_ != CaptainStates::Idle)
  {
    std::string msg("Impossible to enable external mission. Something is already running");
    ROS_ERROR_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Store caller name
  external_mission_caller_name_ = event.getCallerName();

  // Set captain state
  state_ = CaptainStates::ExternalMission;

  res.message = std::to_string(external_mission_feedback_id_);  // The message is used to pass the id
  res.success = true;
  ROS_INFO_STREAM("External mission enabled. Feedback id: " << res.message);
  return true;
}

bool Captain::disableExternalMissionSrv(
    ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>& event)
{
  // Get service response
  std_srvs::Trigger::Response& res = event.getResponse();

  // Check current state
  if (state_ != CaptainStates::ExternalMission)
  {
    std::string msg("Impossible to disable external mission. Captain not in external mission state");
    ROS_WARN_STREAM(msg);
    res.message = msg;
    res.success = false;
    return true;
  }

  // Disable external mission if stopped from somewhere else
  if (external_mission_caller_name_ != event.getCallerName())
  {
    std::string srv_name(external_mission_caller_name_ + "/disable");
    ros::ServiceClient* srv_client = new ros::ServiceClient(nh_.serviceClient<std_srvs::Trigger>(srv_name));
    if (srv_client->waitForExistence(ros::Duration(5.0)))
    {
      std_srvs::Trigger* params = new std_srvs::Trigger();
      bool* success = new bool();
      if (cola2::ros::callServiceWithTimeout(*srv_client, params->request, params->response, *success, 5.0))
      {
        if (!success)
        {
          ROS_ERROR_STREAM("Trigger service call failed");
        }
        else
        {
          if (!params->response.success)
          {
            ROS_WARN_STREAM("External disable service " << srv_name
                                                        << " responded False with msg: " << params->response.message);
          }
        }
        delete srv_client;
        delete params;
        delete success;
      }
      else
      {
        ROS_ERROR_STREAM("Service " << srv_name << " did not return after a 5 sec. timeout. "
                                    << "Detaching waiting thread and leaking memory");
      }
    }
    else
    {
      ROS_ERROR_STREAM("External disable service " << srv_name << " does not exist");
      delete srv_client;
    }
  }

  // Set captain state
  state_ = CaptainStates::Idle;

  // Increment id
  ++external_mission_feedback_id_;

  res.message = "External mission disabled";
  res.success = true;
  ROS_INFO_STREAM(res.message);
  return true;
}

bool Captain::disableAllAndSetIdleSrv(ros::ServiceEvent<std_srvs::Trigger::Request, std_srvs::Trigger::Response>& event)
{
  std_srvs::Trigger::Request req = event.getRequest();
  if (state_ == CaptainStates::Goto)
  {
    disableGotoSrv(req, event.getResponse());
  }
  else if (state_ == CaptainStates::Section)
  {
    disableSectionSrv(req, event.getResponse());
  }
  else if (state_ == CaptainStates::Mission)
  {
    // Reuse disable mission, but adapt request and response
    cola2_msgs::Mission::Request mission_req;
    cola2_msgs::Mission::Response mission_res;
    disableMissionSrv(mission_req, mission_res);
    event.getResponse().message = mission_res.message;
    event.getResponse().success = mission_res.success;
  }
  else if (state_ == CaptainStates::KeepPosition)
  {
    disableKeepPositionSrv(req, event.getResponse());
  }
  else if (state_ == CaptainStates::SafetyKeepPosition)
  {
    disableSafetyKeepPositionSrv(req, event.getResponse());
  }
  else if (state_ == CaptainStates::ExternalMission)
  {
    disableExternalMissionSrv(event);
  }
  else
  {
    std::string msg("Already in Idle state");
    ROS_INFO_STREAM(msg);
    event.getResponse().message = msg;
    event.getResponse().success = true;
  }

  // Disable all paused missions too
  while (!loaded_missions_.empty())
  {
    std::string mission_name = loaded_missions_.begin()->first;
    executePendingActions(mission_name);
    deleteLoadedMission(mission_name);
  }

  return true;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "captain");
  Captain captain;
  ros::spin();
  return 0;
}
