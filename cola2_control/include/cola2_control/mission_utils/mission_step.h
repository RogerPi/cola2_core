/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_CONTROL_MISSION_STEP_H
#define COLA2_CONTROL_MISSION_STEP_H

#include <cola2_control/mission_utils/mission_action.h>
#include <cola2_control/mission_utils/mission_maneuver.h>
#include <iostream>
#include <memory>
#include <vector>

/**
 * \brief MissionStep class from mission types.
 */
class MissionStep
{
private:
  std::shared_ptr<MissionManeuver> maneuver_;
  std::vector<MissionAction> actions_;

public:
  MissionStep();

  ~MissionStep();

  std::shared_ptr<MissionManeuver> getManeuverPtr() const;

  std::vector<MissionAction> getActions() const;

  void setManeuverPtr(std::shared_ptr<MissionManeuver> maneuver);

  void addAction(const MissionAction& action);
};

#endif  // COLA2_CONTROL_MISSION_STEP_H
