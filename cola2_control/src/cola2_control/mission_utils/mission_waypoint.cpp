/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <cola2_control/mission_utils/mission_waypoint.h>

MissionWaypoint::MissionWaypoint() : MissionManeuver(WAYPOINT_MANEUVER)
{
}

MissionWaypoint::~MissionWaypoint()
{
}

MissionWaypoint::MissionWaypoint(const MissionPosition& position, const double speed, const MissionTolerance& tolerance)
  : MissionManeuver(WAYPOINT_MANEUVER), position_(position), speed_(speed), tolerance_(tolerance)
{
}

double MissionWaypoint::x()
{
  return position_.getLatitude();
}

double MissionWaypoint::y()
{
  return position_.getLongitude();
}

double MissionWaypoint::z()
{
  return position_.getZ();
}

MissionPosition MissionWaypoint::getPosition() const
{
  return position_;
}

double MissionWaypoint::getSpeed() const
{
  return speed_;
}

MissionTolerance MissionWaypoint::getTolerance() const
{
  return tolerance_;
}

void MissionWaypoint::setPosition(const MissionPosition& position)
{
  position_ = position;
}

void MissionWaypoint::setSpeed(const double speed)
{
  speed_ = speed;
}

void MissionWaypoint::setTolerance(const MissionTolerance& tolerance)
{
  tolerance_ = tolerance;
}
