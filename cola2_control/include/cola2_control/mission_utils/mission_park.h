/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_CONTROL_MISSION_PARK_H
#define COLA2_CONTROL_MISSION_PARK_H

#include <cola2_control/mission_utils/mission_maneuver.h>
#include <cola2_control/mission_utils/mission_position.h>
#include <cola2_control/mission_utils/mission_tolerance.h>
#include <iostream>

/**
 * \brief MissionPark class from mission types.
 */
class MissionPark : public MissionManeuver
{
private:
  MissionPosition position_;
  unsigned int time_;
  MissionTolerance tolerance_;

public:
  MissionPark();

  ~MissionPark();

  MissionPark(const MissionPosition& position, const unsigned int t, const MissionTolerance& tolerance);

  //  friend std::ostream& operator<<(std::ostream& stream, const MissionPark& p)
  //  {
  //    stream << "Park -> " << p.getPosition() << " for " << p.getTime() << "s";
  //  }

  MissionPosition getPosition() const;

  unsigned int getTime() const;

  MissionTolerance getTolerance() const;

  void setPosition(const MissionPosition& position);

  void setTime(const unsigned int time);

  void setTolerance(const MissionTolerance& tolerance);

  double x();

  double y();

  double z();
};

#endif  // COLA2_CONTROL_MISSION_PARK_H
