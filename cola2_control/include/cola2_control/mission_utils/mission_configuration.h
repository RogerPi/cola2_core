/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_CONTROL_MISSION_CONFIGURATION_H
#define COLA2_CONTROL_MISSION_CONFIGURATION_H

#include <iostream>
#include <string>

/**
 * \brief MissionConfiguration class from mission types.
 */
class MissionConfiguration
{
private:
  std::string key_;
  std::string value_;

public:
  MissionConfiguration();

  ~MissionConfiguration();

  MissionConfiguration(const std::string& key, const std::string& value);

  friend std::ostream& operator<<(std::ostream& stream, const MissionConfiguration& conf)
  {
    stream << "configuration -> " << conf.getKey() << ": " << conf.getValue();
    return stream;
  }

  std::string getKey() const;

  std::string getValue() const;
};

#endif  // COLA2_CONTROL_MISSION_CONFIGURATION_H
