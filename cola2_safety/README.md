# COLA2 SAFETY

This is a ROS package with nodes to perform safety checkings to the COLA2 architecture.

[TOC]

[//]: <> (recovery_actions start)

## recovery_actions

**Node**: /recovery_actions

This node is used to handle requests for vehicle recovery actions coming from all nodes.

![recovery_actions](doc/readme/recovery_actions.svg)

**Publishers**:

* /controller/thruster_setpoints [[cola2_msgs/Setpoints](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/Setpoints.html)]
* /recovery_actions/external_recovery_action [[cola2_msgs/RecoveryAction](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/RecoveryAction.html)]

**Subscribers**:

* /vehicle_status [[cola2_msgs/VehicleStatus](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/VehicleStatus.html)]

**Services**:

* /recovery_actions/recover [[cola2_msgs/Recovery](http://api.iquarobotics.com/201910/api/cola2_msgs/html/srv/Recovery.html)]

**Parameters**:

* /recovery_actions/emergency_surface_setpoints
* /recovery_actions/frame_id

[//]: <> (recovery_actions end)

[//]: <> (safe_depth_altitude start)

## safe_depth_altitude

**Node**: /safe_depth_altitude

This node prevents the vehicle to go below a maximum depth or a minimum altitude by sending a BodyVelocityReq asking a negative heave with maximum priority.

![safe_depth_altitude](doc/readme/safe_depth_altitude.svg)

**Publishers**:

* /controller/body_velocity_req [[cola2_msgs/BodyVelocityReq](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/BodyVelocityReq.html)]
* /diagnostics [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]

**Subscribers**:

* /navigator/navigation [[cola2_msgs/NavSts](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/NavSts.html)]

**Services**: None


**Parameters**:

* /safe_depth_altitude/max_depth
* /safe_depth_altitude/min_altitude

[//]: <> (safe_depth_altitude end)

[//]: <> (safety_supervisor start)

## safety_supervisor

**Node**: /safety_supervisor

This node receives the vehicle status message and checks a set of rules aimed at detecting errors in the system (low battery level, water leak sensors, data gap in a navigation sensor, etc.). If any of the rule checks is triggered, the safety supervisor calls a recovery action, which acts appropriately depending on the type of error.

![safety_supervisor](doc/readme/safety_supervisor.svg)

**Publishers**:

* /diagnostics [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]
* /safety_supervisor/status [[cola2_msgs/SafetySupervisorStatus](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/SafetySupervisorStatus.html)]

**Subscribers**:

* /recovery_actions/external_recovery_action [[cola2_msgs/RecoveryAction](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/RecoveryAction.html)]
* /vehicle_status [[cola2_msgs/VehicleStatus](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/VehicleStatus.html)]

**Services**:

* /safety_supervisor/reload_params [[std_srvs/Trigger](http://docs.ros.org/melodic/api/std_srvs/html/srv/Trigger.html)]

**Parameters**:

* /safety_supervisor/max_temperatures_values
* /safety_supervisor/min_altitude_update
* /safety_supervisor/min_battery_charge
* /safety_supervisor/min_battery_voltage
* /safety_supervisor/min_depth_update
* /safety_supervisor/min_dvl_good_data
* /safety_supervisor/min_dvl_update
* /safety_supervisor/min_gps_update
* /safety_supervisor/min_imu_update
* /safety_supervisor/min_modem_update
* /safety_supervisor/min_nav_update
* /safety_supervisor/min_wifi_update
* /safety_supervisor/timeout

[//]: <> (safety_supervisor end)

[//]: <> (set_zero_velocity start)

## set_zero_velocity

**Node**: /set_zero_velocity

This node makes the vehicle keep velocities at zero if it is below a configured depth and teleoperation is giving only disabled setpoints.

![set_zero_velocity](doc/readme/set_zero_velocity.svg)

**Publishers**:

* /controller/body_velocity_req [[cola2_msgs/BodyVelocityReq](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/BodyVelocityReq.html)]

**Subscribers**:

* /controller/body_force_req [[cola2_msgs/BodyForceReq](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/BodyForceReq.html)]
* /controller/body_velocity_req [[cola2_msgs/BodyVelocityReq](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/BodyVelocityReq.html)]
* /controller/world_waypoint_req [[cola2_msgs/WorldWaypointReq](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/WorldWaypointReq.html)]
* /navigator/navigation [[cola2_msgs/NavSts](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/NavSts.html)]

**Services**: None


**Parameters**:

* /set_zero_velocity/set_zero_velocity_axis
* /set_zero_velocity/set_zero_velocity_depth

[//]: <> (set_zero_velocity end)

[//]: <> (vehicle_status_parser start)

## vehicle_status_parser

**Node**: /vehicle_status_parser

This node subscribes to the aggregated diagnostics message and publishes a VehicleStatus message summarizing the current status of the vehicle.

![vehicle_status_parser](doc/readme/vehicle_status_parser.svg)

**Publishers**:

* /diagnostics [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]
* /vehicle_status [[cola2_msgs/VehicleStatus](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/VehicleStatus.html)]

**Subscribers**:

* /captain/captain_status [[cola2_msgs/CaptainStatus](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/CaptainStatus.html)]
* /captain/state_feedback [[cola2_msgs/CaptainStateFeedback](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/CaptainStateFeedback.html)]
* /diagnostics_agg [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]
* /navigator/navigation [[cola2_msgs/NavSts](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/NavSts.html)]
* /watchdog/elapsed_time [[std_msgs/Int32](http://docs.ros.org/melodic/api/std_msgs/html/msg/Int32.html)]

**Services**: None


**Parameters**:

* /vehicle_status_parser/altitude_data_age
* /vehicle_status_parser/battery_charge
* /vehicle_status_parser/battery_voltage
* /vehicle_status_parser/depth_data_age
* /vehicle_status_parser/dvl_data_age
* /vehicle_status_parser/dvl_valid_data_age
* /vehicle_status_parser/gps_data_age
* /vehicle_status_parser/imu_data_age
* /vehicle_status_parser/inside_virtual_cage
* /vehicle_status_parser/modem_data_age
* /vehicle_status_parser/navigation_data_age
* /vehicle_status_parser/temperature
* /vehicle_status_parser/temperature_name
* /vehicle_status_parser/thrusters_enabled
* /vehicle_status_parser/vehicle_initialized
* /vehicle_status_parser/water
* /vehicle_status_parser/wifi_data_age

[//]: <> (vehicle_status_parser end)

[//]: <> (virtual_cage start)

## virtual_cage

**Node**: /virtual_cage

This node checks if the vehicle moves beyond some given virtual limits defined in NED coordinates and publishes a diagnostic message.

![virtual_cage](doc/readme/virtual_cage.svg)

**Publishers**:

* /diagnostics [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]
* /virtual_cage/markers/cage [[visualization_msgs/Marker](http://docs.ros.org/melodic/api/visualization_msgs/html/msg/Marker.html)]

**Subscribers**:

* /navigator/navigation [[cola2_msgs/NavSts](http://api.iquarobotics.com/201910/api/cola2_msgs/html/msg/NavSts.html)]

**Services**: None


**Parameters**:

* /virtual_cage/calls_keep_position
* /virtual_cage/east_longitude
* /virtual_cage/east_origin
* /virtual_cage/enable
* /virtual_cage/enabled
* /virtual_cage/north_longitude
* /virtual_cage/north_origin

[//]: <> (virtual_cage end)

[//]: <> (watchdog start)

## watchdog

**Node**: /watchdog

This node keeps track of the COLA2 running time. It is used by the safety supervisor to check it against the safety timeout and trigger and abort and surface recovery action.

![watchdog](doc/readme/watchdog.svg)

**Publishers**:

* /diagnostics [[diagnostic_msgs/DiagnosticArray](http://docs.ros.org/melodic/api/diagnostic_msgs/html/msg/DiagnosticArray.html)]
* /watchdog/elapsed_time [[std_msgs/Int32](http://docs.ros.org/melodic/api/std_msgs/html/msg/Int32.html)]

**Subscribers**: None


**Services**:

* /watchdog/reset_timeout [[std_srvs/Trigger](http://docs.ros.org/melodic/api/std_srvs/html/srv/Trigger.html)]

**Parameters**: None


[//]: <> (watchdog end)
