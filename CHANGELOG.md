# Changelog

## [3.2.4] - 18-03-2020

* `cola2_control`: Corrected issue in section services.

## [3.2.3] - 30-01-2020

* `cola2_nav`: Corrected magnetic declination sign so the magnetic declination is properly taken into account into the navigation filter. Magnetic declinations from websites such as [http://www.magnetic-declination.com/](http://www.magnetic-declination.com/) can be now used with the sign given there.

## [3.2.2] - 29-01-2020

* `cola2_safety`: Solved issue in disable mission service of recovery actions node.

## [3.2.1] - 10-01-2020

* `cola2_nav`: Added missing dependency to be able to compile without installing cola2_lib in the system (related to cola2_lib hotfix 3.2.2).

## [3.2.0] - 22-10-2019

* Add `_node` to all nodes source code files
* Add tags to `README.md` for auto-documentation
* Apply `cola2_lib` refactor changes
* Change `std_srvs/Emtpy` services to `std_srvs/Trigger`
* Delete node name from example configurations
* Keep nodes alive for auto-documentation
* `cola2_comms`: new acoustic command to reset vehicle timeout
* `cola2_control`: added mission pause and resume services
* `cola2_control`: added state_feedback topic to publish information about on-going state for all services
* `cola2_control`: all captain services are nonblocking now
* `cola2_control`: captain service calls now have a timeout of 5 seconds to ensure captain is not blocked
* `cola2_control`: captain_node is now single-threaded
* `cola2_control`: changed captain_status topic to reflect the internal state of the captain and the list of loaded missions
* `cola2_control`: enable/disable mission services can enable/dsable a mission by name
* `cola2_control`: improve checks on mission validity before starting the mission
* `cola2_control`: keyboard_node is now single-threaded
* `cola2_control`: removed default mission nonblocking service from captain
* `cola2_log`: adapt default_param_handler to take filename as node name
* `cola2_log`: default_param_handler service to update single parameter
* `cola2_nav`: add service to only reload NED info
* `cola2_nav`: delete navigator/altitude_filtered topic (altitude output of navigatior now only in navigator/navigation)
* `cola2_nav`: solve bug in too old USBL updates
* `cola2_sim`: dynamics, solve bug in negative thruster setpoints

## [3.1.1] - 15-04-2019

* Solved bug when loading mission actions with parameters

## [3.1.0] - 25-02-2019

* First release
