/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <cola2_control/mission_utils/mission_configuration.h>

MissionConfiguration::MissionConfiguration()
{
}

MissionConfiguration::MissionConfiguration(const std::string& key, const std::string& value) : key_(key), value_(value)
{
}

MissionConfiguration::~MissionConfiguration()
{
}

std::string MissionConfiguration::getKey() const
{
  return key_;
}

std::string MissionConfiguration::getValue() const
{
  return value_;
}
